#!/usr/bin/env python

from distutils.core import setup

setup(
    name="d1",
    version="1.0.0",
    packages=["d1"],
    install_requires=["aiohttp", "cryptography", "dnspython"],
    entry_points={"console_scripts": ["d1=d1:cli"]},
)
