import asyncio
import logging
import secrets
import sys
from pathlib import Path

from configparser import ConfigParser

from d1.context import Context
from d1.webhook import webhook_send
from d1.errors import BaseError


def random_subdomain() -> str:
    """Generate a random subdomain, 10 letters long."""
    return secrets.token_urlsafe().replace("-", "")[:10].lower()


async def _do_nameservers(ctx):
    """Check nameservers for all wildcards."""
    tot = len(ctx.wildcards)
    to_remove = []

    for idx, (dom_id, domain) in enumerate(ctx.wildcards):
        print(ctx.ignore_ns_check)
        if domain in ctx.ignore_ns_check:
            print(f"ignoring ns check on {dom_id} {domain}")
            continue

        try:
            print(f"checking ns ({idx + 1} / {tot}) ({domain})")
            await ctx.check_ns(domain)
        except BaseError as err:
            to_remove.append(dom_id)
            await webhook_send(ctx, err, domain=(dom_id, domain))

    return to_remove


async def _check_addrs(ctx, domains: dict):
    """Check IP address leaks on all domains."""
    tot = len(domains)

    for idx, (dom_id, domain) in enumerate(domains.items()):
        if domain in ctx.ignore_ip_check:
            print(f"ignoring ip check on {dom_id} {domain}")
            continue

        try:
            print(f"checking addresses ({idx + 1} / {tot}) " f"({domain})")

            await ctx.check_addr_dns(domain)
        except BaseError as err:
            domains.pop(dom_id)
            await webhook_send(ctx, err, domain=(dom_id, domain))


async def _main_check(ctx, domains: dict):
    """Main check with d1's protocol."""
    tot = len(domains)

    for idx, (dom_id, domain) in enumerate(domains.items()):
        try:
            print(f"querying domains ({idx + 1} / {tot}) " f"({domain})")

            await ctx.validate(domain)
        except BaseError as err:
            await webhook_send(ctx, err, domain=(dom_id, domain))


async def _main_d1(d1ctx):
    print("domains", await d1ctx.domains)
    print("wildcards", d1ctx.wildcards)

    to_remove = []

    if d1ctx.nameservers:
        to_remove = await _do_nameservers(d1ctx)
        print("ns check finish")

    # from this point on we need to convert
    # the "*" in wildcards to actual subdomains
    # that resolve to something, that way we can
    # check their A records.

    domains = await d1ctx.domains
    treated_domains = {}

    for dom_id, domain in domains.items():
        treated_domains[dom_id] = domain.replace("*", random_subdomain())

    for dom_id in to_remove:
        try:
            treated_domains.pop(dom_id)
            print("removed id", dom_id, ": untrustable")
        except KeyError:
            pass

    await _check_addrs(d1ctx, treated_domains)
    print("addr check finish")

    await _main_check(d1ctx, treated_domains)


async def main():
    """Main function"""
    try:
        config_path = Path(sys.argv[1]).resolve()
    except IndexError:
        print("no config path specified")
        return
    except FileNotFoundError:
        print("file not found")
        return

    config_raw = ConfigParser()
    config_raw.read(config_path)

    d1_context = Context(config_raw["d1"], config_raw["d1:unwanted_ips"])

    print("hello world", d1_context)

    try:
        await _main_d1(d1_context)
    finally:
        await d1_context.close()

    await d1_context.close()


def cli():
    """Main CLI Entrypoint."""
    logging.basicConfig(level=logging.INFO)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
