class BaseError(Exception):
    description = "general error"

    @property
    def message(self):
        """Return the error's message."""
        return self.args[0]

    @property
    def possible_cause(self):
        """Return the possible cause of an error."""
        return self.args[1]


class DNSError(BaseError):
    description = "dns error"


class NameserverError(DNSError):
    description = "nameserver verification failure"


class AddressLeak(DNSError):
    description = "unwanted ip being leaked"


class QueryError(BaseError):
    description = "error on main query check"
