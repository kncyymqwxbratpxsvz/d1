d1
========

d1 is a domain checker for any elixi.re instance. The basic guarantee of d1
is that all the domains under an instance are pointing to the *right* instance
and not some other, hijacked instance.

## Deploying

First, you will need to generated a secret key that is shared between d1
and your elixi.re instance. You will need the `cryptography` Python package.

Requirements:
 - Python 3.6+

In your shell:
```bash
pip3 install --user --editable .

cp config.example.ini config.ini
```

In the python console:
```python
>>> from cryptography.fernet import Fernet
>>> Fernet.generate_key()
b'...'
```

The generated key must be placed on d1's and elixire's respective
configuration files. **It must be the same key.**

Edit `config.ini` as wanted.

Run
```bash
d1 path/to/your/config.ini
```
to test it and run in prod.

It is preferred to create a cron job so that d1 regularly checks
without manual intervention. As a recommendation run it every day.


## Protocol spec

Crypto primitives can be found [here](https://cryptography.io/en/latest/fernet/#implementation).

### Setup

 - Create a shared secret key between the elixire instance and d1.
    We'll call this `SK`.
 - Make sure you know the `INSTANCE_URL` of your elixire instance;
    d1 uses this domain to determine its external IP.

### Checking a domain

 - Generate random data (`RD`), encrypt it as follows: `encrypt(RD, SK)`.
 - Send a request to `/api/check` with `encrypt(RD, SK)`
    - The elixire instance decrypts it, gets `RD`, and the request's IP address.
    - Replies with `encrypt(RD || IP, SK)`.
 - Decrypt response, and compare. If `RD` matches and `IP` matches the
    setup's IP address, the domain is genuine.
